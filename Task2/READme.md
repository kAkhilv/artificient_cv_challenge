### Task Description
<br>
Use the following augmentation methods on the sample image under data/sample.png<br>
and save the result under this path: 'data/sample_augmented.png'<br>

Note:<br>
    - use torchvision.transforms<br>
    - use the following augmentation methods with the same order as below:<br>
        * affine: degrees: ±5, 
                  translation= 0.1 of width and height, 
                  scale: 0.9-1.1 of the original size<br>
        * rotation ±5 degrees,<br>
        * horizontal flip with a probablity of 0.5<br>
        * center crop with height=320 and width=640<br>
        * resize to height=160 and width=320<br>
        * color jitter with:  brightness=0.5,
                              contrast=0.5, 
                              saturation=0.4, 
                              hue=0.2<br>
    - use default values for anything unspecified<br>

### Image Augmentation
#### Libraries
[torchvision](https://pytorch.org/vision/stable/transforms.html) version: 0.15.0 <br>
[Pillow](https://pillow.readthedocs.io/en/stable/) version: 9.3.0 <br>
[OpenCV](https://docs.opencv.org/4.7.0/) version: 4.7.0.72 <br>
### Original Image
![Image before torch transformations](sample.png "Original Image")

### Augmented Image
![Image after torch transformations](sample_augmented.png "Augmented Image")
