"""
develop a model based on the onnx file in model/model.onnx 

Note:
    - initialize the convolutions layer with uniform xavier
    - initialize the linear layer with a normal distribution (mean=0.0, std=1.0)
    - initialize all biases with zeros
    - use batch norm wherever is relevant
    - use random seed 8
    - use default values for anything unspecified
"""

import numpy as np
import torch
import torch.nn as nn



torch.manual_seed(8)    # DO NOT MODIFY!
np.random.seed(8)   # DO NOT MODIFY!


# write your code here ...

input_tensor = torch.randn(1,3,160,320)  #Input Tensor to the convolutional net


class MyConvolutionNet(nn.Module):
    def __init__(self):
        super(MyConvolutionNet, self).__init__()
        
        #Check the Netron image to obtain the attributes and shape of each convolution
        
        #Conv block 1
        self.conv1 = nn.Sequential(nn.Conv2d(3,32,3,padding=1,stride=1),  #Declaring the convolutional layers
                                             nn.BatchNorm2d(32))  #Batch Normalization
        
        #Conv block 2
        self.conv2 = nn.Sequential(nn.Conv2d(32,64,3,stride=2,padding=1),
                                   nn.BatchNorm2d(64))
                                   
        #Conv block 3
        self.conv3 = nn.Sequential(nn.Conv2d(64,64,3,padding = 1,stride = 1),
                                   nn.BatchNorm2d(64))
                                   
       
        #Conv block 4
        self.conv4 = nn.Sequential(nn.Conv2d(64, 128,3,padding =1, stride=2),
                                   nn.BatchNorm2d(128))
                                   
         
        #Conv block 5
        self.conv5 = nn.Sequential(nn.Conv2d(128,64,1,padding =0 , stride=1),
                                   nn.BatchNorm2d(64))
                                   
        
        #Conv block 6
        self.conv6 = nn.Sequential(nn.Conv2d(64,64,3,padding =1, stride=1),
                                   nn.BatchNorm2d(64))
                                   
        
        #Conv block 7
        self.conv7 = nn.Sequential(nn.Conv2d(64,64,3,padding=1, stride=1),
                                   nn.BatchNorm2d(64))
        
        
        #Conv block 8
        self.conv8 = nn.Sequential(nn.Conv2d(64,64,3,padding=1, stride=1),
                                   nn.BatchNorm2d(64))
         
        #Conv block 9a
        self.conv9a = nn.Sequential(nn.Conv2d(128,64,1,padding=0, stride=1),
                                    nn.BatchNorm2d(64))
    
        
        #Conv block 9b
        self.conv9b = nn.Sequential(nn.Conv2d(64,64,3,padding=1, stride=1),
                                    nn.BatchNorm2d(64))
        
    
        #Conv block 10
        self.conv10 = nn.Sequential(nn.Conv2d(256,256,kernel_size=1,stride=1,padding=0),
                                    nn.BatchNorm2d(256))
        
        
        
        #Conv block 11
        self.conv11 = nn.Sequential(nn.Conv2d(256,128,kernel_size=1,stride=1,padding=0),
                                    nn.BatchNorm2d(128))
        
        
        #Conv block 12a
        self.conv12a = nn.Sequential(nn.Conv2d(256,128,kernel_size=1,stride=1,padding=0),
                                     nn.BatchNorm2d(128))
                                     
        
        
        #Conv block 12b
        self.conv12b = nn.Sequential(nn.Conv2d(128,128,kernel_size=3,stride=2,padding=1),
                                     nn.BatchNorm2d(128))
        
        self.Sigmoid = nn.Sigmoid()  #Activation function
        
        
        self.max_pool = nn.MaxPool2d(2, stride=2, padding=0)#Max Pool
        
        
        #Linear Layer
        self.linear = nn.Linear(256,256)
        
        
        self.weights_init() #Initalising the weights_init ffunction
    
    def weights_init(self):
        for m in self.modules():   #Inititalise the weights basing on the condition for Conv or Linear
            
            if isinstance(m, nn.Conv2d):
                nn.init.xavier_uniform_(m.weight) #xavier uniform weights for the convolution layer
                nn.init.zeros_(m.bias)  #bias initialised to zero
            elif isinstance(m, nn.Linear):
                nn.init.normal_(m.weight, mean=0.0, std=1.0) #Weights for linear layer according to a normal distribution
                nn.init.zeros_(m.bias)
            
        
    def forward(self,x):
        x1 = self.conv1(x)
        x = self.Sigmoid(x1)
        x = torch.mul(x1,x)

        x2 = self.conv2(x)
        x = self.Sigmoid(x2)
        x = torch.mul(x2,x)

        x3 = self.conv3(x)
        x = self.Sigmoid(x3)
        x = torch.mul(x3,x)

        x4 = self.conv4(x)
        x = self.Sigmoid(x4)
        x_s1 = torch.mul(x4,x) # x_s1 towards the left side of the split

        x5 = self.conv5(x_s1)
        x = self.Sigmoid(x5)
        x_s2 = torch.mul(x5,x) # x_s2 towards the left split

        x6 = self.conv6(x_s2)
        x = self.Sigmoid(x6)
        x = torch.mul(x6,x)
        
        x7 = self.conv7(x)
        x = self.Sigmoid(x7)
        x_s3 = torch.mul(x7,x)  #x_s3 towards the left split

        x8 = self.conv8(x_s3)
        x = self.Sigmoid(x8)
        x = torch.mul(x8,x)

        x9b = self.conv9b(x)
        x = self.Sigmoid(x9b)
        x_s4 = torch.mul(x9b,x)

        x9a = self.conv9a(x_s1)
        x = self.Sigmoid(x9a)
        x_1 = torch.mul(x9a,x)
        
        x = torch.cat((x_1, x_s2, x_s3,x_s4), dim=1) # Four lines meeting concatenating
        
        x10 = self.conv10(x)
        x = self.Sigmoid(x10)
        x_s5 = torch.mul(x10,x)  #x_s5 to go through max pooling
        
        x11 = self.conv11(x_s5)
        x = self.Sigmoid(x11)
        x_s6 = torch.mul(x11,x)
        
        
        x_s5_pooled = self.max_pool(x_s5) #Max_pooling operation
        
        x12_a = self.conv12a(x_s5_pooled)
        x = self.Sigmoid(x12_a)
        x_7 = torch.mul(x12_a,x) #Left split
        
        
        x12_b = self.conv12b(x_s6)
        x = self.Sigmoid(x12_b)
        x_s6 = torch.mul(x12_b,x) # Right split
        
        
        x = torch.cat((x_7, x_s6), dim=1) #Concatenation of the arrays
        
        x_transposed = x.permute(0,2,3,1) #Perform transpose operation

        x_reshaped = x_transposed.view(-1, 256) ## Reshaping the array
        
        
        x = self.linear(x_reshaped) #applying the linear layer
        
        x = x.view(1, 20, 40, 256) # last reshape
        
        x = x.permute(0,3,1,2)
        
        output = self.Sigmoid(x)
        
        return output
    
Conv_layer = MyConvolutionNet()  #Initialising the Net
Conv_layer.eval()

output = Conv_layer(input_tensor) #Passing the randomly generated input tensor
print("Shape of the output from MyConvolutionNet is {}".format(output.shape))        
#Output: [1,256,20,40]



#Checking the output with that of ONNX model output


import onnxruntime as rt
model_path = "model.onnx"
session = rt.InferenceSession(model_path)

input_name = session.get_inputs()[0].name
dummy_input = input_tensor.numpy().astype(np.float32)

output2 = session.run(None, {input_name:dummy_input})
print("Shape of the output from ONNX model is {}".format(output2[0].shape)) 
#Output: [1,256,20,40]
        

        
        
        
        
        
        
        
