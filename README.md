# Artificient_CV_challenge

Number of Tasks = 3 <br>
Please find the description, code and files in the corresponding task folders <br>

### Python Libraries and Versions
[Python](https://www.python.org/) version 3.10.11 <br>
[Pytorch](https://pytorch.org/) version: 2.0.0 - py3.10_cpu_0
<br>
[numpy](https://numpy.org/) version: 1.24.3
<br>

