"""
Write a code using pytorch to replicate a grouped 2D convolution layer based on the original 2D convolution. 

The common way of using grouped 2D convolution layer in Pytorch is to use 
torch.nn.Conv2d(groups=n), where n is the number of groups.

However, it is possible to use a stack of n torch.nn.Conv2d(groups=1) to replicate the same
result. The weights must be copied and be split between the convs in the stack.

You can use:
    - use default values for anything unspecified  
    - all available functions in NumPy and Pytorch
    - the custom layer must be able to take all parameters of the original nn.Conv2d 
"""

import numpy as np
import torch
import torch.nn as nn


torch.manual_seed(8)    # DO NOT MODIFY!
np.random.seed(8)   # DO NOT MODIFY!

# random input (batch, channels, height, width)

x = torch.randn(2, 64, 100, 100)

# original 2d convolution
grouped_layer = nn.Conv2d(64, 128, 3, stride=1, padding=1, groups=16, bias=True)

# weights and bias
w_torch = grouped_layer.weight
b_torch = grouped_layer.bias

y = grouped_layer(x) #Output from the original Convolutional layer


#Declaring the Custom Grouped Layers
class CustomGroupedConv2D(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size, stride=1, padding=1, dilation=1, groups =1, bias = True, padding_mode = 'zeros', device = None):
        super(CustomGroupedConv2D,self).__init__()
        self.groups = groups
        self.in_channels = in_channels
        self.out_channels = out_channels
        self.in_channels_groups = self.in_channels // self.groups #dividing the in and out channels according to groups
        self.out_channels_groups = self.out_channels // self.groups
        self.convstacklayer = nn.ModuleList()  #Declaring a list in type torch
        for _ in range(groups):
            self.convstacklayer.append(nn.Conv2d(in_channels = self.in_channels_groups, 
                                        out_channels = self.out_channels_groups,
                                        kernel_size = kernel_size,
                                        stride = stride,
                                        padding = padding,
                                        bias = bias))
        for i, conv in enumerate(self.convstacklayer):  #Copy and split the weights and biases to their corresponding convs in the stack
            start_idx = i * self.out_channels_groups  #indexes chosen accordingly to select a range of out_channels // groups  for each divided tensor
            end_idx = (i+1) * self.out_channels_groups
            conv.weight.data = w_torch.data[start_idx:end_idx, :, :, :]
            conv.bias.data = b_torch.data[start_idx:end_idx]
            
        
    def forward(self, x):  #defining the forward pass
        x_split = torch.split(x,x.shape[1] // self.groups, dim =1) #split the tensor based on groups
        output = torch.cat([conv(x_split[i]) for i,conv in enumerate(self.convstacklayer)], dim =1) #Pass the splitted tensor seperately to perform convolution and concatenate the result in the end
        return output


Custom_layer = CustomGroupedConv2D(in_channels=64, out_channels=128, kernel_size=3, groups=16) #Initializing the Custom 2D convolution layer

custom_y = Custom_layer(x)   #Output from the Custom Convolution layer


#check for similarity in shape and value
print(y.shape)
#Output: torch.Size([2, 128, 100, 100])

print(custom_y.shape)
#Output: torch.Size([2, 128, 100, 100])

print(torch.isclose(y, custom_y,rtol=1e-05, atol=1e-08)) #Comparison using torch.isclose with default tolerance for floating point values


#Output: Tensor[True]




    
  