### Task Description

"""
Write a code using pytorch to replicate a grouped 2D convolution layer based on the original 2D convolution. 

The common way of using grouped 2D convolution layer in Pytorch is to use 
torch.nn.Conv2d(groups=n), where n is the number of groups.

However, it is possible to use a stack of n torch.nn.Conv2d(groups=1) to replicate the same
result. The weights must be copied and be split between the convs in the stack.

You can use:
    - use default values for anything unspecified  
    - all available functions in NumPy and Pytorch
    - the custom layer must be able to take all parameters of the original nn.Conv2d 
"""

#### Libraries and References


[Pytorch Conv2d](https://pytorch.org/docs/stable/generated/torch.nn.Conv2d.html)
<br>
[ResNext Paper](https://arxiv.org/abs/1611.05431) to understand and visualise Grouped Convolutions
